﻿using UnityEditor;
using UnityEngine;

using TestProject.Test.TestStatusList;

namespace TestProjectTest.Editor.TestStatusList.Editor {
    [CustomEditor(typeof(UseStatusListMenu))]
    public class UseStatusListEditor : UnityEditor.Editor {
        public override void OnInspectorGUI() {
            DrawDefaultInspector();

            if (GUILayout.Button("Find")) {
                Test();
            }
        }

        public void Test() {
            var tester = target as UseStatusListMenu;
            if (tester.CharacterName is null) {
                Debug.LogWarning("キャラクター名が入力されていない");
                return;
            }
            if (tester.StatusList is null) {
                Debug.LogWarning("ステータスリストがアタッチされていない");
                return;
            }

            var status = tester.StatusList.FindByName(tester.CharacterName);
            if (status is null) {
                Debug.Log($"データが存在しませんでした: {tester.CharacterName}");
                return;
            }

            Debug.Log(
                $"{status.name}: " +
                $"MaxHp({status.maxHealth}) " +
                $"Attack({status.attack}) " +
                $"Defence({status.defence})"
            );
        }
    }
}
