﻿using UnityEngine;

using TestProject.Infrastructure.ImportFile.CharacterStatus;

namespace TestProject.Test.TestStatusList {
    [CreateAssetMenu(menuName = "ScriptableObject/TEST/UseStatusList")]
    public class UseStatusListMenu : ScriptableObject {
        public string CharacterName;
        public StatusList StatusList;
    }
}
