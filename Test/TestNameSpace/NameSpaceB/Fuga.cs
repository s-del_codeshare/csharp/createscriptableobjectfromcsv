﻿using UnityEngine;

namespace TestProject.Test.TestNameSpace.NameSpaceB {
    /// <summary>
    /// NameSpaceB の Fuga<br />
    /// UseFuga から呼び出される
    /// </summary>
    public class Fuga {
        public void Info() {
            Debug.Log($"{typeof(Fuga)}.Info()");
        }
    }
}
