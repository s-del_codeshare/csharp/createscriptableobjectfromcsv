﻿using UnityEngine;

// クラス名 Fuga が衝突するのでエイリアスで解決している
using FugaA = TestProject.Test.TestNameSpace.NameSpaceA.Fuga;
using FugaB = TestProject.Test.TestNameSpace.NameSpaceB.Fuga;

namespace TestProject.Test.TestNameSpace {
    /// <summary>
    /// NameSpace A と B の Fuga を利用する
    /// </summary>
    public class UseFuga : MonoBehaviour {
        public void Start() {
            var fugaA = new FugaA();
            var fugaB = new FugaB();
            fugaA.Info();
            fugaB.Info();
        }
    }
}
