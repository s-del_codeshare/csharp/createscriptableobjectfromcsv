﻿using UnityEngine;

namespace TestProject.Test.TestNameSpace.NameSpaceA {
    /// <summary>
    /// NameSpaceA の Hoge<br />
    /// オブジェクトにアタッチして利用する
    /// </summary>
    public class Hoge : MonoBehaviour {
        public void Start() {
            Debug.Log($"{typeof(Hoge)}.Start()");
        }
    }
}
