﻿using System.Collections.Generic;
using UnityEngine;

namespace TestProject.Infrastructure.ImportFile.CharacterStatus {
    [CreateAssetMenu(menuName = "ScriptableObject/StatusList")]
    public class StatusList : ScriptableObject {
        public List<CharacterStatus> list = new List<CharacterStatus>();

        public void Add(CharacterStatus status) { 
            list.Add(status);
        }

        public CharacterStatus FindByName(string name) {
            return list.Find(line => line.name == name);
        }
    }
}
