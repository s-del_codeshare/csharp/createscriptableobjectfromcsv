﻿using System;

namespace TestProject.Infrastructure.ImportFile.CharacterStatus {
    [Serializable]
    public class CharacterStatus {
        public string name;
        public int maxHealth;
        public int attack;
        public int defence;
    }
}
