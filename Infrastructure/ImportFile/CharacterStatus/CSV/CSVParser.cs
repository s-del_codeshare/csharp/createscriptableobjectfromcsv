﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace TestProject.Infrastructure.ImportFile.CharacterStatus.CSV {
    public class CSVParser {
        public static readonly int HEADER_INDEX = 1;

        readonly string[] header;
        readonly string[][] body;

        public CSVParser(string text) {
            var lines = DelimitByLine(text);
            if (lines.Length <= 1) {
                throw new ArgumentException("CSV の内容が無いよう");
            }

            header = SplitByComma(lines.Take(HEADER_INDEX).First());
            body = lines.Skip(HEADER_INDEX)
                        .Where(line => !String.IsNullOrWhiteSpace(line))
                        .Select(SplitByComma)
                        .ToArray();
        }

        public static string[] DelimitByLine(string text) {
            return text.Split('\n');
        }

        public static string[] SplitByComma(string line) {
            return Regex.Split(line, @", *");
        }

        public string[] Header() {
            return header;
        }

        public string[][] Body() {
            return body;
        }
    }
}
