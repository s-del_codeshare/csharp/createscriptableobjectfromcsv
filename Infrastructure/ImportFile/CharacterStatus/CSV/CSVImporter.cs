﻿using UnityEngine;

namespace TestProject.Infrastructure.ImportFile.CharacterStatus.CSV {
    [CreateAssetMenu(menuName = "ScriptableObject/CSVImporter")]
    public class CSVImporter : ScriptableObject {
        public TextAsset CSVFile;
    }
}
