﻿using UnityEditor;
using UnityEngine;

namespace TestProject.Infrastructure.ImportFile.CharacterStatus.CSV.Editor {
    [CustomEditor(typeof(CSVImporter))]
    public class CSVImporterEditor : UnityEditor.Editor {
        public static readonly string BASE_PATH = "Assets/ImportedData";

        public override void OnInspectorGUI() {
            DrawDefaultInspector();

            if (GUILayout.Button("データ読み込み")) {
                ToScriptableObject();
            }
        }

        public void ToScriptableObject() {
            var importer = target as CSVImporter;
            if (importer.CSVFile is null) {
                Debug.LogWarning($"{importer.name}: csv が指定されていない");
                return;
            }

            var list = CreateInstance<StatusList>();
            var parser = new CSVParser(importer.CSVFile.text);

            foreach(var line in parser.Body()) {
                var status = new CharacterStatus {
                    name = line[0],
                    maxHealth = int.Parse(line[1]),
                    attack = int.Parse(line[2]),
                    defence = int.Parse(line[3])
                };
                list.Add(status);
            }

            var path = $"{BASE_PATH}/{importer.CSVFile.name}.asset";
            var asset = AssetDatabase.LoadAssetAtPath<StatusList>(path);
            if (asset is not null) {
                AssetDatabase.DeleteAsset(path);
            }

            AssetDatabase.CreateAsset(list, path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            Debug.Log("変換完了");
        }
    }
}
